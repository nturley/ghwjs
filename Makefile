default: ghwlib.js

ghwlib.js : ghdl/src/grt/ghwlib.c
	emcc $< -o $@ -s EXPORTED_FUNCTIONS="['_ghw_open', '_ghw_read_base', '_ghw_get_hie_name', '_ghw_disp_hie', '_print_name', '_ghw_read_sm', '_ghw_disp_values']" -s EXTRA_EXPORTED_RUNTIME_METHODS='["ccall", "cwrap", "FS", "getValue", "Pointer_stringify"]' -s WASM=0

test : ghwlib.js
	node test.js

clean:
	rm ghwlib.js