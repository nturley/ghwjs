fs = require('fs')
clib = require('./ghwlib.js')


class Ghw {
    constructor(fname) {
        const ghw_handler_size = 72
        const tempname = 'file.ghw'
        const b = fs.readFileSync(fname)
        clib.FS.writeFile(tempname, b)
        this._ptr = clib._malloc(ghw_handler_size)
        var ret = clib.ccall('ghw_open', 'number', ['number', 'string'], [this._ptr, tempname])
        var ret = clib.ccall('ghw_read_base', 'number', ['number'], [this._ptr])
        this._num_sigs_ptr = this._ptr + 43
        this._sigs_ptr = this._ptr + 55
        this._hie_ptr = this._ptr + 59
        this._time_ptr = this._ptr + 64
    }

    signals() {
        if (this._sigs == undefined) {
            var num_sigs = clib.getValue(this._num_sigs_ptr, 'i32')
            var sigarray = clib.getValue(this._sigs_ptr, 'i32')
            this._sigs = []
            for (var i=0;i<num_sigs-1;i++) {
                var sig_addr = sigarray + (i + 1) * 8
                this._sigs.push(new Signal(this._ptr, sig_addr))
            }
        }
        return this._sigs
    }

    hierarchy() {
        if (this._hier_top == undefined) {
            this._hier_top = new HierarchyElement(this, clib.getValue(this._hie_ptr, 'i32'), null)
        }
        return this._hier_top
    }

    time() {
        return clib.getValue(this._time_ptr, 'i64')
    }

    init_cycle() {
        this._sm = clib._malloc(4)
        // init
        this._res = clib.ccall('ghw_read_sm', 'number', ['number', 'number'], [this._ptr, this._sm])
        // snapshot
        this._res = clib.ccall('ghw_read_sm', 'number', ['number', 'number'], [this._ptr, this._sm])
    }

    next_cycle() {
        if (this._res != 2) {
            return
        }
        this._res = clib.ccall('ghw_read_sm', 'number', ['number', 'number'], [this._ptr, this._sm])
    }

    trace_sigs() {
        var cycles = []
        this.init_cycle()
        var lastvals = []
        while (this._res == 2) {
            var sigvals = []
            this.signals().forEach(function(s) {
                sigvals.push(s.get_val())
            })
            var changes = []
            for (var i=0; i < sigvals.length; i++) {
                if (sigvals[i] != lastvals[i]) {
                    changes.push({'signal':i,'value':sigvals[i]})
                }
            }
            lastvals = sigvals
            var cycle = {
                "time":this.time(),
                "changes":changes
            }
            cycles.push(cycle)
            this.next_cycle()
        }
        return cycles
    }

    dump() {
        return {
            "hierarchy":this.hierarchy().dump(),
            "transitions":this.trace_sigs()
        }
    }
}

class Signal{
    constructor(ghw_handle, ptr) {
        this.ghw_handle = ghw_handle
        this.ptr = ptr
    }

    get_val() {
        var ttype =clib.getValue(this._type(), 'i32')
        if (ttype == 22 || ttype == 23) {
            var lits = clib.getValue(this._type()+16, 'i32')
            var index = clib.getValue(this._val(), 'i32') * 4
            var lit = clib.getValue(lits+index,'i32')
            return clib.Pointer_stringify(lit)
        }
        if (ttype == 25) {
            return clib.getValue(this._val(), 'i32')
        }
        return 'I dont know'
    }

    _type() {
        return clib.getValue(this.ptr, 'i32')
    }

    _val() {
        return clib.getValue(this.ptr+4, 'i32')
    }
}

class HierarchyElement {
    constructor(ghw_handle, ptr, parent) {
        this.ghw_handle = ghw_handle
        this.parent = parent
        this._ptr = ptr
        this._name_offset = 8
        this._sibling_offset = 12
        this._child_offset = 16
        this._sigs_offset = 20
        this._children = null
    }

    name() {
        return clib.Pointer_stringify(clib.getValue(this._ptr + this._name_offset, 'i32'))
    }

    kind_str() {
        return clib.ccall('ghw_get_hie_name', 'string', ['number'], [this._ptr])
    }

    _kind_int() {
        return clib.getValue(this._ptr, 'i32')
    }

    children() {
        if (this._children == null) {
            this._children = []
            if (!this._has_children()){
                return this._children
            }
            var current = this._first_child()
            this._children.push(current)
            while (current._has_sibling()) {
                var next = current._next_sibling()
                this._children.push(next)
                current = next
            }
        }
        return this._children
    }

    _has_sigs() {
        return this._kind_int() > 15
    }

    sigs() {
        var ret = []
        if (!this._has_sigs()) {
            return ret
        }
        var sig_addr = clib.getValue(this._ptr + this._sigs_offset, 'i32')
        var value = clib.getValue(sig_addr, 'i32')
        while (value != 0) {
            ret.push(value)
            sig_addr += 4
            value = clib.getValue(sig_addr, 'i32')
        }
        return ret
    }

    _has_children() {

        if (this._kind_int() > 13) {
            return false
        } else {
            return clib.getValue(this._ptr + this._child_offset, 'i32') != 0
        }
    }

    _has_sibling() {
        const ret = clib.getValue(this._ptr + this._sibling_offset, 'i32')
        return ret != 0
    }

    _next_sibling() {
        return new HierarchyElement(this.ghw_handle, clib.getValue(this._ptr + this._sibling_offset, 'i32'), this.parent)
    }

    _first_child() {
        return new HierarchyElement(this.ghw_handle, clib.getValue(this._ptr + this._child_offset, 'i32'), this)
    }

    dump() {
        var children = []
        this.children().forEach(function(c) {
            children.push(c.dump())
        })
        return {
            "name":this.name(),
            "type":this.kind_str(),
            "signals":this.sigs(),
            "children":children
        }
    }
}

exports.Ghw = Ghw