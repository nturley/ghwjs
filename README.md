# Waveform Information

Hierarchy Element
* name (string)
* signals (list of ints)
* type (string)
* children (list of Hierarchy Elements)

Signal Change
* Signal Index (int)
* value (string)

Transition
* Timestamp (int)
* signal changes (list of signal changes)